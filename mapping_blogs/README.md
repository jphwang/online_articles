# Mapping blogs
This article is intended show users how to create marked maps using plotly

## Data
- loc_data.csv includes data of all unique locations used, including the coordinates and location type
- The CSV files from data_csvs are locations mentioned in various blog posts about Sydney.

### Article
* Link: https://towardsdatascience.com/interactive-maps-with-python-pandas-and-plotly-following-bloggers-through-sydney-c24d6f30867e